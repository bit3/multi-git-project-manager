MultiGitProjectManager
======================

MGPM helps you manage hundreds of projects to keep in sync with their remote repository.

Quick start
-----------

(1) download and install the [latest build][page]:

Go to [bit3.gitlab.io/multi-git-project-manager][page] and download your preferred package.

(2) create a new directory to store the projects in:

```bash
$ mkdir ~/my-workspace
```

(3) put a `mgpm.yml` into this workspace:

```bash
$ echo 'repositories:
  -
    type: github
    owner: bit3
' > ~/my-projects/mgpm.yml
```

(see [docs][page] for more detailed example)

(4) start mgpm in the workspace:

```bash
$ cd ~/my-workspace
$ ~/path/to/mgpm-2.0.0/bin/mgpm -i
```

HAVE FUN!!!

[page]: https://bit3.gitlab.io/multi-git-project-manager/
