package io.bit3.mgpm.cli;

import com.jcraft.jsch.IdentityRepository;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.agentproxy.AgentProxyException;
import com.jcraft.jsch.agentproxy.Connector;
import com.jcraft.jsch.agentproxy.RemoteIdentityRepository;
import com.jcraft.jsch.agentproxy.USocketFactory;
import com.jcraft.jsch.agentproxy.connector.PageantConnector;
import com.jcraft.jsch.agentproxy.connector.SSHAgentConnector;
import com.jcraft.jsch.agentproxy.usocket.NCUSocketFactory;
import io.bit3.mgpm.cmd.Args;
import io.bit3.mgpm.config.Config;
import io.bit3.mgpm.config.RepositoryConfig;
import io.bit3.mgpm.terminal.TerminalCredentialsProvider;
import io.bit3.mgpm.worker.*;
import org.eclipse.jgit.transport.CredentialsProviderUserInfo;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.util.FS;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static org.fusesource.jansi.Ansi.ansi;

public class CliApplication {
  private final Logger logger = LoggerFactory.getLogger(CliApplication.class);
  private final Args args;
  private final Config config;
  private final PrintStream output;
  private final Workers workers;
  private final Terminal terminal;
  private final LineReader lineReader;
  private final JschConfigSessionFactory jschConfigSessionFactory;

  public CliApplication(Args args, Config config) throws IOException {
    AnsiConsole.systemInstall();

    this.args = args;
    this.config = config;
    this.output = AnsiConsole.out();
    this.workers = new Workers();
    this.terminal = TerminalBuilder.builder().name("mgpm").system(true).jansi(true).build();
    this.lineReader = LineReaderBuilder.builder().appName("mgpm").terminal(terminal).build();
    this.jschConfigSessionFactory = createJschConfigSessionFactory(lineReader);

    JSch.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
    SshSessionFactory.setInstance(jschConfigSessionFactory);
  }

  public void run() {
    List<File> knownDirectories = new LinkedList<>();

    ExecutorService executor = Executors.newFixedThreadPool(args.getThreads());

    for (final RepositoryConfig repositoryConfig : config.getRepositories()) {
      knownDirectories.add(repositoryConfig.getDirectory());

      Worker worker = new Worker(config, repositoryConfig, args.isDoInit(), args.isDoUpdate());
      worker.registerObserver(new LoggingWorkerObserver());
      worker.registerObserver(new CliWorkerObserver());
      executor.submit(worker);
    }
    executor.shutdown();

    while (!executor.isTerminated()) {
      workers.rotateSpinner();

      try {
        Thread.sleep(200);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        break;
      }
    }

    workers.deleteSpinner();

    if (args.isShowStatus() && !args.isOmitSuperfluousWarnings()) {
      printSuperfluousDirectories(knownDirectories);
    }
  }

  private JschConfigSessionFactory createJschConfigSessionFactory(LineReader lineReader) {
    return new JschConfigSessionFactory() {
      @Override
      protected void configure(OpenSshConfig.Host host, Session session) {
        session.setUserInfo(new CredentialsProviderUserInfo(
                session,
                new TerminalCredentialsProvider(lineReader)
        ));
      }

      @Override
      protected JSch createDefaultJSch(FS fs) throws JSchException {
        final JSch jsch = super.createDefaultJSch(fs);

        try {
          createJschAgentConnector().ifPresent(connector -> {
            IdentityRepository identityRepository = new RemoteIdentityRepository(connector);
            jsch.setIdentityRepository(identityRepository);
          });
        } catch (AgentProxyException e) {
          logger.warn("Failed to initialize jsch agent connector", e);
        }

        return jsch;
      }
    };
  }

  private Optional<Connector> createJschAgentConnector() throws AgentProxyException {
    if (SSHAgentConnector.isConnectorAvailable()) {
      USocketFactory usf = new NCUSocketFactory();
      return Optional.of(new SSHAgentConnector(usf));
    } else if (PageantConnector.isConnectorAvailable()) {
      return Optional.of(new PageantConnector());
    } else {
      return Optional.empty();
    }
  }

  private void printSuperfluousDirectories(List<File> knownDirectories) {
    Set<File> parentDirectories = knownDirectories
        .stream()
        .map(File::getParentFile)
        .collect(Collectors.toCollection(TreeSet::new));
    Set<File> seenFiles = new TreeSet<>();

    for (File parentDirectory : parentDirectories) {
      File[] files = parentDirectory.listFiles();
      if (null != files)
        Collections.addAll(seenFiles, files);
    }

    // remove known directories (=> directories with managed repositories) from seen files
    seenFiles.removeAll(knownDirectories);
    knownDirectories.stream().map(this::collectParents).forEach(seenFiles::removeAll);

    // remove mgpm.yml from seen files
    seenFiles = seenFiles.stream().filter(f -> !"mgpm.yml".equals(f.getName())).collect(Collectors.toSet());

    URI workingDirectory = Paths.get(".").toAbsolutePath().normalize().toUri();
    for (File file : seenFiles) {
      String relativePath = workingDirectory.relativize(file.toURI()).getPath();
      relativePath = relativePath.replaceFirst("/$", "");
      output.printf(
              " * %s %s%n",
              ansi().fgYellow().a(relativePath).reset(),
              ansi().fgRed().a("superfluous").reset()
      );
    }
  }

  private List<File> collectParents(File file) {
    final List<File> parents = new LinkedList<>();

    File previousParentFile = file;
    File parentFile = file.getParentFile();
    while (null != parentFile && !Objects.equals(previousParentFile, parentFile)) {
      parents.add(parentFile);
      previousParentFile = parentFile;
      parentFile = parentFile.getParentFile();
    }

    return parents;
  }

  private class CliWorkerObserver extends AbstractWorkerObserver {
    @Override
    public void start(Worker worker) {
      workers.addActiveWorker(worker.getRepositoryConfig().getPathName(), "...");
    }

    @Override
    public void activity(Activity activity, Worker worker) {
      workers.addActiveWorker(worker.getRepositoryConfig().getPathName(), activity.getMessage());
    }

    @Override
    public void end(Worker worker) {
      workers.removeActiveWorker(worker.getRepositoryConfig().getPathName());

      try {
        List<String> localBranchNames = worker.getLocalBranchNames();
        Map<String, List<String>> remoteBranchNames = worker.getRemoteBranchNames();

        if (localBranchNames.isEmpty() && (!args.isShowStatus() || remoteBranchNames.isEmpty())) {
          return;
        }

        Map<String, List<String>> addedRemoteBranchNames = worker.getAddedRemoteBranchNames();
        Map<String, List<String>> deletedRemoteBranchNames = worker.getDeletedRemoteBranchNames();
        Map<String, Upstream> branchUpstreamMap = worker.getBranchUpstreamMap();
        Map<String, Update> branchUpdateStatus = worker.getBranchUpdateStatus();
        Map<String, FromToIsh> branchUpdateIsh = worker.getBranchUpdateIsh();
        Map<String, Worker.Stats> branchStats = worker.getBranchStats();
        Map<String, List<String>> remoteBranchesUsedAsUpstream = new HashMap<>();

        int padding = localBranchNames.stream().mapToInt(String::length).max().orElse(1);
        String pattern = "%-" + padding + "s";
        boolean printDetails = true;

        if (!logger.isInfoEnabled()) {
          printDetails = !addedRemoteBranchNames.isEmpty()
                  || !deletedRemoteBranchNames.isEmpty()
                  || !branchStats.values().stream().map(Worker.Stats::isEmpty).reduce(true, (a, b) -> a && b);
        } else if (!logger.isWarnEnabled()) {
          // be quiet
          return;
        }

        synchronized (output) {
          if (!printDetails) {
            return;
          }

          workers.deleteSpinner();

          output.printf(
                  " * %s%n",
                  ansi().fgYellow().a(worker.getRepositoryConfig().getPathName()).reset()
          );

          for (String branchName : localBranchNames) {
            Upstream upstream = branchUpstreamMap.get(branchName);
            Update update = branchUpdateStatus.get(branchName);
            Worker.Stats stats = branchStats.get(branchName);
            String headSymbolicRef = worker.getHeadSymbolicRef();

            if (null != upstream) {
              remoteBranchesUsedAsUpstream
                      .computeIfAbsent(upstream.getRemoteName(), k -> new LinkedList<>())
                      .add(upstream.getRemoteBranch());
            }

            printBranchName(pattern, branchName, headSymbolicRef);
            printBranchUpstream(upstream);
            printBranchUpdate(branchName, update, branchUpdateIsh);
            printBranchStats(stats);

            output.println();
          }

          if (args.isShowStatus()) {
            printRemoteBranches(pattern, remoteBranchNames, addedRemoteBranchNames, deletedRemoteBranchNames, remoteBranchesUsedAsUpstream);
          }

          output.println();
        }
      } catch (Exception exception) {
        logger.error(exception.getMessage(), exception);
      }
    }

    private void printBranchName(String pattern, String branchName, String headSymbolicRef) {
      output.print("   ");

      boolean isHead = branchName.equals(headSymbolicRef);
      if (isHead) {
        output.print(ansi().fgBlue().a("> ").reset());
      } else {
        output.print("  ");
      }

      output.printf(pattern, branchName);
    }

    private void printBranchUpstream(Upstream upstream) {
      if (null != upstream) {
        output.print(ansi().fgBrightBlack().a(String.format(" → %s", upstream.getRemoteRef())).reset());
      }
    }

    private void printBranchUpdate(String branchName, Update update, Map<String, FromToIsh> branchUpdateIsh) {
      if (null != update) {
        final Ansi ansi = ansi();
        switch (update) {
          case SKIP_NO_UPSTREAM:
            return;

          case SKIP_UPSTREAM_DELETED:
          case SKIP_CONFLICTING:
            ansi.fgBrightRed();
            break;

          case UP_TO_DATE:
          case MERGED_FAST_FORWARD:
          case REBASED:
            ansi.fgGreen();
            break;

          default:
            ansi.fgYellow();
        }

        output.printf(
                " %s",
                ansi.a(update.toString().toLowerCase().replace('_', ' ')).reset()
        );

        if (update == Update.MERGED_FAST_FORWARD || update == Update.REBASED) {
          FromToIsh fromToIsh = branchUpdateIsh.get(branchName);
          output.printf(
                  " %s..%s",
                  fromToIsh.getFrom().getObjectId().abbreviate(8).name(),
                  fromToIsh.getTo().getObjectId().abbreviate(8).name()
          );
        }
      }
    }

    private void printBranchStats(Worker.Stats stats) {
      if (null != stats) {
        printCommitsStats(stats);
        printAddedStats(stats);
        printModifiedStats(stats);
        printCopiedStats(stats);
        printDeletedStats(stats);
        printCleanStat(stats);
      }
    }

    private void printCommitsStats(Worker.Stats stats) {
      if (0 == stats.getCommitsBehind() && 0 == stats.getCommitsAhead() && stats.isClean()) {
        output.print(ansi().fgGreen().a("  ✔").reset());
      }

      if (stats.getCommitsBehind() > 0) {
        output.print(ansi().fgCyan().a("  ↓").a(stats.getCommitsBehind()).reset());
      }

      if (stats.getCommitsAhead() > 0) {
        output.print(ansi().fgCyan().a("  ↑").a(stats.getCommitsAhead()).reset());
      }
    }

    private void printAddedStats(Worker.Stats stats) {
      if (stats.getAdded() > 0) {
        output.print(ansi().fgRed().a("  +").a(stats.getAdded()).reset());
      }
    }

    private void printModifiedStats(Worker.Stats stats) {
      if (stats.getModified() > 0) {
        output.print(ansi().fgYellow().a("  ★").a(stats.getModified()).reset());
      }
    }

    private void printCopiedStats(Worker.Stats stats) {
      if (stats.getChanged() > 0) {
        output.print(ansi().fgMagenta().a("  ↷").a(stats.getChanged()).reset());
      }
    }

    private void printDeletedStats(Worker.Stats stats) {
      if (stats.getDeleted() > 0) {
        output.print(ansi().fgMagenta().a("  -").a(stats.getDeleted()).reset());
      }
    }

    private void printCleanStat(Worker.Stats stats) {
      if (!stats.isClean()) {
        output.print(ansi().fgBrightRed().a("  ⚡").reset());
      }
    }

    private void printRemoteBranches(String pattern, Map<String, List<String>> remoteBranchNames, Map<String, List<String>> addedRemoteBranchNames, Map<String, List<String>> deletedRemoteBranchNames, Map<String, List<String>> remoteBranchesUsedAsUpstream) {
      for (Map.Entry<String, List<String>> entry : remoteBranchNames.entrySet()) {
        printRemoteBranch(pattern, addedRemoteBranchNames, deletedRemoteBranchNames, remoteBranchesUsedAsUpstream, entry);
      }
    }

    private void printRemoteBranch(String pattern, Map<String, List<String>> addedRemoteBranchNames, Map<String, List<String>> deletedRemoteBranchNames, Map<String, List<String>> remoteBranchesUsedAsUpstream, Map.Entry<String, List<String>> entry) {
      String remoteName = entry.getKey();
      List<String> currentBranchNames = entry.getValue();
      List<String> addedBranchNames = addedRemoteBranchNames.get(remoteName);
      List<String> deletedBranchNames = deletedRemoteBranchNames.get(remoteName);

      Set<String> remoteBranches = new TreeSet<>(currentBranchNames);
      if (null != addedBranchNames) {
        remoteBranches.addAll(addedBranchNames);
      }
      if (null != deletedBranchNames) {
        remoteBranches.addAll(deletedBranchNames);
      }

      for (String remoteBranch : remoteBranches) {
        boolean usedAsUpstream = remoteBranchesUsedAsUpstream.containsKey(remoteName)
            && remoteBranchesUsedAsUpstream.get(remoteName).contains(remoteBranch);

        if (usedAsUpstream) {
          continue;
        }

        output.printf(
                "   %s",
                ansi().fgBrightBlack().a(String.format(pattern, remoteName + "/" + remoteBranch)).reset()
        );

        boolean wasAdded = null != addedBranchNames
            && addedBranchNames.contains(remoteBranch);

        boolean wasDeleted = null != deletedBranchNames
            && deletedBranchNames.contains(remoteBranch);

        if (wasAdded) {
          output.print(ansi().fgGreen().a(" (added)").reset());
        } else if (wasDeleted) {
          output.print(ansi().fgRed().a(" (removed)").reset());
        }

        output.println();
      }
    }
  }
}
