package io.bit3.mgpm.cli;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.fusesource.jansi.Ansi.ansi;

public class Workers {
    private static final PrintStream out = AnsiConsole.out();
    private static final String[] spinnerCharacters = new String[]{"|", "/", "-", "\\"};

    private Map<String, String> activeWorkers = new LinkedHashMap<>();
    private int spinnerIndex = 0;
    private int writtenLines = 0;

    public Workers() {
        AnsiConsole.systemInstall();
    }

    public synchronized void addActiveWorker(String label, String activity) {
        activeWorkers.put(label, activity);
    }

    public synchronized void removeActiveWorker(String label) {
        activeWorkers.remove(label);
    }

    public synchronized void rotateSpinner() {
        deleteSpinner();

        if (!Ansi.isEnabled() || activeWorkers.isEmpty()) {
            return;
        }

        int localSpinnerIndex = spinnerIndex;
        for (Map.Entry<String, String> entry: activeWorkers.entrySet()) {
            String label = entry.getKey();
            String activity = entry.getValue();

            out.printf(" (%s) %s: %s%n", spinnerCharacters[localSpinnerIndex], label, activity);

            localSpinnerIndex = (localSpinnerIndex + 1) % spinnerCharacters.length;
            writtenLines++;
        }

        spinnerIndex = (spinnerIndex + 1) % spinnerCharacters.length;
    }

    public synchronized void deleteSpinner() {
        if (!Ansi.isEnabled() || 0 == writtenLines) {
            return;
        }

        // restore cursor position
        out.print(ansi().cursorUp(writtenLines).eraseScreen(Ansi.Erase.FORWARD));
        writtenLines = 0;
    }

}
