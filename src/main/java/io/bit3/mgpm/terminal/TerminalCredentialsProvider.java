package io.bit3.mgpm.terminal;

import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;
import org.jline.reader.LineReader;

import java.util.stream.Stream;

import static org.fusesource.jansi.AnsiConsole.out;

@SuppressWarnings({"unused", "WeakerAccess"})
public class TerminalCredentialsProvider extends CredentialsProvider {
    private final LineReader lineReader;

    public TerminalCredentialsProvider(LineReader lineReader) {
        this.lineReader = lineReader;
    }

    @Override
    public boolean isInteractive() {
        return true;
    }

    @Override
    public boolean supports(CredentialItem... items) {
        return Stream.of(items).anyMatch(item ->
                !(item instanceof CredentialItem.StringType
                        || item instanceof CredentialItem.CharArrayType
                        || item instanceof CredentialItem.YesNoType
                        || item instanceof CredentialItem.InformationalMessage)
        );
    }

    @Override
    public boolean get(URIish uri, CredentialItem... items) {
        for (CredentialItem item : items) {
            if (item instanceof CredentialItem.StringType) {
                if (item.getPromptText().contains("Password") || item.getPromptText().contains("Passphrase")) {
                    final String password = lineReader.readLine(item.getPromptText() + ": ", '*');
                    ((CredentialItem.StringType) item).setValue(password);
                } else {
                    final String username = lineReader.readLine(item.getPromptText() + ": ");
                    ((CredentialItem.StringType) item).setValue(username);
                }
            } else if (item instanceof CredentialItem.CharArrayType) {
                final String password = lineReader.readLine(item.getPromptText() + ": ", '*');
                ((CredentialItem.CharArrayType) item).setValue(password.toCharArray());
            } else if (item instanceof CredentialItem.YesNoType) {
                final String answer = lineReader.readLine(item.getPromptText());
                ((CredentialItem.YesNoType) item).setValue("y".equalsIgnoreCase(answer)
                        || "yes".equalsIgnoreCase(answer));
            } else if (item instanceof CredentialItem.InformationalMessage) {
                out.println(item.getPromptText());
            } else {
                throw new UnsupportedCredentialItem(uri, item.getClass().getName() + ":" + item.getPromptText());
            }
        }
        return false;
    }
}
