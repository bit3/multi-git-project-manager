package io.bit3.mgpm.worker;

import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.submodule.SubmoduleWalk;
import org.eclipse.jgit.transport.RemoteConfig;
import org.eclipse.jgit.transport.URIish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class GitAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(GitAdapter.class);

    private final Git git;

    public GitAdapter(File dir) throws IOException {
        this.git = Git.open(dir);
    }

    public GitAdapter(Git git) {
        this.git = git;
    }

    public static GitAdapter clone(String url, File directory) throws GitAPIException {
        final CloneCommand cloneCommand = Git.cloneRepository();
        cloneCommand.setURI(url);
        cloneCommand.setDirectory(directory);
        final Git git = cloneCommand.call();
        return new GitAdapter(git);
    }

    public List<URIish> getRemoteUris(String remoteName) throws GitAPIException {
        final List<RemoteConfig> remoteConfigs = git.remoteList().call();
        return remoteConfigs.stream()
                .filter(remoteConfig -> remoteName.equals(remoteConfig.getName()))
                .findFirst()
                .map(RemoteConfig::getURIs)
                .orElseGet(Collections::emptyList);
    }

    public void setRemoteUris(String origin, URIish uri) throws GitAPIException {
        final RemoteSetUrlCommand remoteSetUrlCommand = git.remoteSetUrl();
        remoteSetUrlCommand.setName(origin);
        remoteSetUrlCommand.setUri(uri);
        remoteSetUrlCommand.call();
    }

    public void initAndUpdateSubmodules() throws GitAPIException {
        git.submoduleInit().call();
        git.submoduleUpdate().call();
    }

    public void syncAndUpdateSubmodules() throws GitAPIException {
        git.submoduleSync().call();
        git.submoduleUpdate().call();
    }

    public String currentBranch() throws IOException {
        return git.getRepository().getBranch();
    }

    public Ref findRef(String ref) throws IOException {
        return git.getRepository().findRef(ref);
    }

    public Status getStatus(SubmoduleWalk.IgnoreSubmoduleMode mode) throws GitAPIException {
        final StatusCommand statusCommand = git.status();
        statusCommand.setIgnoreSubmodules(mode);
        return statusCommand.call();
    }

    public void stash(String message) throws GitAPIException {
        final StashCreateCommand stashCreateCommand = git.stashCreate();
        stashCreateCommand.setIndexMessage(message);
        stashCreateCommand.setIncludeUntracked(true);
        stashCreateCommand.call();
    }

    public void unstash() throws GitAPIException {
        final StashApplyCommand stashApplyCommand = git.stashApply();
        stashApplyCommand.call();
    }

    public Map<String, List<String>> listRemoteBranchNames() throws GitAPIException {
        Map<String, List<String>> remoteBranchNames = new HashMap<>();

        final ListBranchCommand listBranchCommand = git.branchList();
        listBranchCommand.setListMode(ListBranchCommand.ListMode.REMOTE);
        final List<Ref> refs = listBranchCommand.call();

        for (Ref ref : refs) {
            if (!ref.isSymbolic() && ref.getName().startsWith("refs/remotes/")) {
                final String[] chunks = ref.getName().substring(13).split("/", 2);
                remoteBranchNames.merge(chunks[0], new ArrayList<>(Collections.singletonList(chunks[1])), (left, right) -> {
                    left.addAll(right);
                    return left;
                });
            }
        }

        return remoteBranchNames;
    }

    public Collection<String> listLocalBranchNames() throws GitAPIException {
        Collection<String> localBranchNames = new LinkedList<>();

        final ListBranchCommand listBranchCommand = git.branchList();
        final List<Ref> refs = listBranchCommand.call();

        for (Ref ref : refs) {
            localBranchNames.add(ref.getName().replace("refs/heads/", ""));
        }

        return localBranchNames;
    }

    public String getBranchRemote(String branchName) {
        final StoredConfig config = git.getRepository().getConfig();
        return config.getString("branch", branchName, "remote");
    }

    public String getBranchMerge(String branchName) {
        final StoredConfig config = git.getRepository().getConfig();
        return config.getString("branch", branchName, "merge");
    }

    public boolean isBranchRebase(String branchName) {
        final StoredConfig config = git.getRepository().getConfig();
        return config.getBoolean("branch", branchName, "rebase", config.getBoolean("pull", "rebase", false));
    }

    public void fetch(String remoteName) throws GitAPIException {
        final FetchCommand fetchCommand = git.fetch();
        fetchCommand.setRemoveDeletedRefs(true);
        fetchCommand.setRemote(remoteName);
        fetchCommand.call();
    }

    public int countCommitsBetween(Ref left, Ref right) throws IncorrectObjectTypeException, MissingObjectException, GitAPIException {
        final LogCommand logCommand = git.log();
        logCommand.addRange(left.getObjectId(), right.getObjectId());
        final Iterable<RevCommit> commits = logCommand.call();
        final AtomicInteger count = new AtomicInteger();
        commits.iterator().forEachRemaining(commit -> count.incrementAndGet());
        return count.get();
    }

    public void checkout(String ref) throws GitAPIException {
        final CheckoutCommand checkout = git.checkout();
        checkout.setName(ref);
        checkout.call();
    }

    public RebaseResult rebase(String ref) throws GitAPIException {
        final RebaseCommand rebaseCommand = git.rebase();
        rebaseCommand.setUpstream(ref);
        return rebaseCommand.call();
    }

    public void rebaseAbort() throws GitAPIException {
        final RebaseCommand rebaseCommand = git.rebase();
        rebaseCommand.setOperation(RebaseCommand.Operation.ABORT);
        rebaseCommand.call();
    }

    public MergeResult merge(Ref ref, MergeCommand.FastForwardMode fastForwardMode) throws GitAPIException {
        final MergeCommand mergeCommand = git.merge();
        mergeCommand.include(ref);
        mergeCommand.setFastForward(fastForwardMode);
        return mergeCommand.call();
    }
}
