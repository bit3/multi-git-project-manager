package io.bit3.mgpm.worker;

import org.fusesource.jansi.AnsiConsole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;

import static org.fusesource.jansi.Ansi.ansi;

public class LoggingWorkerObserver extends AbstractWorkerObserver {
  private final Logger logger = LoggerFactory.getLogger(LoggingWorkerObserver.class);
  private final PrintStream output = AnsiConsole.out();

  @Override
  public void activity(Activity activity, Worker worker) {
    switch (activity.getAction().logLevel) {
      case TRACE:
        if (logger.isTraceEnabled()) {
          break;
        }
        return;

      case DEBUG:
        if (logger.isDebugEnabled()) {
          break;
        }
        return;

      case INFO:
        if (logger.isInfoEnabled()) {
          break;
        }
        return;

      case WARN:
        if (logger.isWarnEnabled()) {
          break;
        }
        return;

      case ERROR:
        if (logger.isErrorEnabled()) {
          break;
        }
        return;
    }

    synchronized (output) {
      output.printf(
              "[LOG] %s %s %s%n",
              worker.getRepositoryConfig().getPathName(),
              ansi().fgYellow().a(String.format(" [%s] ", activity.getAction().toString())).reset(),
              activity.getMessage()
      );
    }
  }
}
