package io.bit3.mgpm.worker;

import io.bit3.mgpm.config.Config;
import io.bit3.mgpm.config.RepositoryConfig;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.RebaseResult;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.submodule.SubmoduleWalk;
import org.eclipse.jgit.transport.URIish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

public class Worker implements Runnable {
  private final Logger logger = LoggerFactory.getLogger(Worker.class);
  private final List<WorkerObserver> observers = new LinkedList<>();
  private final List<Activity> journal = new LinkedList<>();
  private final Config config;
  private final RepositoryConfig repositoryConfig;
  private final boolean cloneIfNotExists;
  private final Set<String> remoteNames = new HashSet<>();
  private final List<String> localBranchNames = new LinkedList<>();
  private final Map<String, List<String>> oldRemoteBranchNames = new HashMap<>();
  private final Map<String, List<String>> remoteBranchNames = new HashMap<>();
  private final Map<String, List<String>> deletedRemoteBranchNames = new HashMap<>();
  private final Map<String, List<String>> addedRemoteBranchNames = new HashMap<>();
  private final Map<String, Upstream> branchUpstreamMap = new HashMap<>();
  private final Map<String, Update> branchUpdateStatus = new HashMap<>();
  private final Map<String, FromToIsh> branchUpdateIsh = new HashMap<>();
  private final Map<String, Stats> branchStats = new HashMap<>();
  private GitAdapter gitAdapter;
  private String headSymbolicRef;
  private Ref headCommitRef;
  private boolean updateExisting;
  private boolean hasStashed = false;

  public Worker(Config config, RepositoryConfig repositoryConfig, boolean cloneIfNotExists, boolean updateExisting) {
    this.config = config;
    this.repositoryConfig = repositoryConfig;
    this.cloneIfNotExists = cloneIfNotExists;
    this.updateExisting = updateExisting;
  }

  public void registerObserver(WorkerObserver observer) {
    observers.add(observer);
  }

  public Config getConfig() {
    return config;
  }

  public RepositoryConfig getRepositoryConfig() {
    return repositoryConfig;
  }

  public List<String> getLocalBranchNames() {
    return localBranchNames;
  }

  public Map<String, List<String>> getRemoteBranchNames() {
    return remoteBranchNames;
  }

  public Map<String, List<String>> getDeletedRemoteBranchNames() {
    return deletedRemoteBranchNames;
  }

  public Map<String, List<String>> getAddedRemoteBranchNames() {
    return addedRemoteBranchNames;
  }

  public Map<String, Upstream> getBranchUpstreamMap() {
    return branchUpstreamMap;
  }

  public Map<String, Update> getBranchUpdateStatus() {
    return branchUpdateStatus;
  }

  public Map<String, FromToIsh> getBranchUpdateIsh() {
    return branchUpdateIsh;
  }

  public Map<String, Stats> getBranchStats() {
    return branchStats;
  }

  public String getHeadSymbolicRef() {
    return headSymbolicRef;
  }

  @Override
  public void run() {
    Thread.currentThread().setName(repositoryConfig.getName());

    for (WorkerObserver observer : observers) {
      observer.start(this);
    }

    try {
      if (cloneOrReconfigureRepository()) {
        determineHead();
        determineRemoteBranches(oldRemoteBranchNames);
        determineLocalBranchesAndUpstreams();
        fetchRemotes();
        determineRemoteBranches(remoteBranchNames);
        calculateRemoteBranchNameChanges();
        determineStats();
        stashChanges();
        updateBranches();
        restoreHead();
        unstashChanges();
      }
    } catch (Exception exception) {
      logger.error(exception.getMessage(), exception);
      journal.add(new Activity(Action.EXCEPTION_OCCURRED, exception.getMessage()));
    }

    for (WorkerObserver observer : observers) {
      observer.end(this);
    }
  }

  /**
   * Clone or reconfigure the repository if necessary.
   */
  private boolean cloneOrReconfigureRepository() throws WorkerException, GitAPIException, URISyntaxException, IOException {
    File directory = repositoryConfig.getDirectory();

    if (directory.exists()) {
      this.gitAdapter = new GitAdapter(repositoryConfig.getDirectory());

      if (!directory.isDirectory()) {
        throw new WorkerException(String.format("ignoring, \"%s\" is not a directory!", directory));
      }

      if (new File(directory, ".git").isDirectory()) {
        List<URIish> actualUrls = gitAdapter.getRemoteUris("origin");
        URIish expectedUrl = new URIish(repositoryConfig.getUrl());

        if (1 != actualUrls.size() || !Objects.equals(actualUrls.get(0), expectedUrl)) {
          activity(Action.UPDATE_REMOTE_URL, "[{}] update remote url", directory);
          gitAdapter.setRemoteUris("origin", expectedUrl);
        }

        return true;
      }

      File[] children = directory.listFiles();
      if (null == children) {
        logger.warn("[{}] ignoring, the directory could not be listed", directory);
        throw new WorkerException(String.format("ignoring, the directory \"%s\" could not be listed", directory));
      }

      if (0 < children.length) {
        logger.warn("[{}] ignoring, the directory is not empty", directory);
        throw new WorkerException(String.format("ignoring, the directory \"%s\" is not empty", directory));
      }
    }

    if (!cloneIfNotExists) {
      activity(Action.ABORT, "not cloned yet");
      return false;
    }

    if (!directory.mkdirs()) {
      activity(Action.ABORT, "could not create directory");
      return false;
    }

    gitAdapter = GitAdapter.clone(repositoryConfig.getUrl(), directory);
    gitAdapter.initAndUpdateSubmodules();

    updateExisting = false;
    return true;
  }

  /**
   * Determine the current HEAD state.
   */
  private void determineHead() throws IOException {
    headSymbolicRef = gitAdapter.currentBranch();
    headCommitRef = gitAdapter.findRef(headSymbolicRef);
  }

  /**
   * Stash changes, if necessary.
   */
  private void stashChanges() throws GitAPIException {
    Status status = gitAdapter.getStatus(SubmoduleWalk.IgnoreSubmoduleMode.ALL);
    if (status.isClean()) {
      return;
    }

    activity(Action.STASH, "stash changes");
    gitAdapter.stash("Stash before mgpm update");
    hasStashed = true;
  }

  /**
   * Unstash changes, if necessary.
   */
  private void unstashChanges() throws GitAPIException {
    if (!hasStashed) {
      return;
    }

    activity(Action.UNSTASH, "apply stash");
    gitAdapter.unstash();
  }

  private void determineRemoteBranches(Map<String, List<String>> remoteBranchNames) throws GitAPIException {
    activity(Action.PARSE_REMOTE_BRANCHES, "parse remote branches");
    remoteBranchNames.putAll(gitAdapter.listRemoteBranchNames());
  }

  private void calculateRemoteBranchNameChanges() {
    for (Map.Entry<String, List<String>> entry : oldRemoteBranchNames.entrySet()) {
      String remoteName = entry.getKey();
      List<String> oldBranchNames = entry.getValue();
      List<String> newBranchNames = remoteBranchNames.get(remoteName);

      if (null == newBranchNames) {
        deletedRemoteBranchNames.put(remoteName, oldBranchNames);
      } else {
        LinkedList<String> deletedBranchNames = new LinkedList<>(oldBranchNames);
        deletedBranchNames.removeAll(newBranchNames);

        if (!deletedBranchNames.isEmpty()) {
          deletedRemoteBranchNames.put(remoteName, deletedBranchNames);
        }
      }
    }


    for (Map.Entry<String, List<String>> entry : remoteBranchNames.entrySet()) {
      String remoteName = entry.getKey();
      List<String> newBranchNames = entry.getValue();
      List<String> oldBranchNames = oldRemoteBranchNames.get(remoteName);

      if (null == oldBranchNames) {
        addedRemoteBranchNames.put(remoteName, newBranchNames);
      } else {
        LinkedList<String> addedBranchNames = new LinkedList<>(newBranchNames);
        addedBranchNames.removeAll(oldBranchNames);

        if (!addedBranchNames.isEmpty()) {
          addedRemoteBranchNames.put(remoteName, addedBranchNames);
        }
      }
    }
  }

  /**
   * Determine local and remote branches and upstreams.
   */
  private void determineLocalBranchesAndUpstreams() throws GitAPIException {
    activity(Action.PARSE_LOCAL_BRANCHES, "parse local branches");
    localBranchNames.addAll(gitAdapter.listLocalBranchNames());

    activity(Action.DETERMINE_UPSTREAMS, "determine branch upstreams");
    for (String branchName : localBranchNames) {
      String remoteName;
      String remoteRef;
      boolean rebase;

      remoteName = gitAdapter.getBranchRemote(branchName);
      remoteRef = gitAdapter.getBranchMerge(branchName);
      rebase = gitAdapter.isBranchRebase(branchName);

      if (StringUtils.isEmpty(remoteName) || StringUtils.isEmpty(remoteRef)) {
        continue;
      }

      String remoteBranch = null;

      if (remoteRef.startsWith("refs/heads/")) {
        remoteBranch = remoteRef.substring(11);
        remoteRef = remoteName + "/" + remoteBranch;
      } else {
        remoteRef = remoteName + "/" + remoteRef;
      }

      remoteNames.add(remoteName);
      branchUpstreamMap.put(branchName, new Upstream(remoteName, remoteBranch, remoteRef, rebase));
    }
  }

  /**
   * Fetch all tracked remotes.
   */
  private void fetchRemotes() throws GitAPIException {
    for (String remoteName : remoteNames) {
      activity(Action.FETCH_REMOTES, "fetch remote {}", remoteName);
      gitAdapter.fetch(remoteName);
    }
  }

  private void determineStats() throws IOException, GitAPIException {
    for (String branchName : localBranchNames) {
      determineStats(branchName);
    }
  }

  private void determineStats(String branchName) throws IOException, GitAPIException {
    Upstream upstream = branchUpstreamMap.get(branchName);

    if (!determineUpstreamIsAvailable(upstream)) {
      return;
    }

    Stats stats = new Stats();

    Ref localRef = gitAdapter.findRef(branchName);
    Ref remoteRef = gitAdapter.findRef(upstream.getRemoteRef());

    stats.commitsBehind = gitAdapter.countCommitsBetween(localRef, remoteRef);

    stats.commitsAhead = gitAdapter.countCommitsBetween(remoteRef, localRef);

    if (Objects.equals(headCommitRef, localRef)) {
      final Status status = gitAdapter.getStatus(SubmoduleWalk.IgnoreSubmoduleMode.ALL);

      stats.added = status.getAdded().size();
      stats.modified = status.getModified().size();
      stats.changed = status.getChanged().size();
      stats.deleted = status.getRemoved().size();
      stats.clean = status.isClean();
    }

    branchStats.put(branchName, stats);
  }

  /**
   * Update all branches.
   */
  private void updateBranches() throws GitAPIException, IOException {
    if (!updateExisting) {
      return;
    }

    for (String branchName : localBranchNames) {
      updateBranch(branchName);
    }
  }

  private void updateBranch(String branchName) throws GitAPIException, IOException {
    if (!updateExisting) {
      return;
    }

    Upstream upstream = branchUpstreamMap.get(branchName);

    if (null == upstream) {
      branchUpdateStatus.put(branchName, Update.SKIP_NO_UPSTREAM);
      return;
    }

    if (!determineUpstreamIsAvailable(upstream)) {
      branchUpdateStatus.put(branchName, Update.SKIP_UPSTREAM_DELETED);
      return;
    }

    activity(Action.CHECKOUT, "checkout branch {}", branchName);
    gitAdapter.checkout(branchName);

    Ref fromIsh = gitAdapter.findRef(branchName);
    Ref toIsh = gitAdapter.findRef(upstream.getRemoteRef());

    if (Objects.equals(fromIsh.getObjectId(), toIsh.getObjectId())) {
      branchUpdateStatus.put(branchName, Update.UP_TO_DATE);
      return;
    }

    branchUpdateIsh.put(branchName, new FromToIsh(fromIsh, toIsh));

    if (upstream.isRebase()) {
      activity(Action.REBASE, "rebase onto {}", upstream.getRemoteRef());

      final RebaseResult rebaseResult = gitAdapter.rebase(upstream.getRemoteRef());
      branchUpdateStatus.put(branchName, Update.REBASED);

      if (RebaseResult.Status.OK != rebaseResult.getStatus() && RebaseResult.Status.FAST_FORWARD != rebaseResult.getStatus()) {
        activity(Action.REBASE_ABORT, "rebase aborted");
        gitAdapter.rebaseAbort();
        branchUpdateStatus.put(branchName, Update.SKIP_CONFLICTING);
      }
    } else {
      activity(Action.MERGE, "merge branch {}", upstream.getRemoteRef());

      Ref remoteRef = gitAdapter.findRef(upstream.getRemoteRef());
      final MergeResult mergeResult = gitAdapter.merge(remoteRef, MergeCommand.FastForwardMode.FF_ONLY);
      branchUpdateStatus.put(branchName, Update.MERGED_FAST_FORWARD);

      if (MergeResult.MergeStatus.ABORTED == mergeResult.getMergeStatus()) {
        activity(Action.MERGE_ABORT, "merge aborted");
        branchUpdateStatus.put(branchName, Update.SKIP_CONFLICTING);
      } else if (MergeResult.MergeStatus.FAST_FORWARD != mergeResult.getMergeStatus() && MergeResult.MergeStatus.ALREADY_UP_TO_DATE != mergeResult.getMergeStatus()) {
        throw new IllegalStateException("Illegal merge status: " + mergeResult.getMergeStatus());
      }
    }

    if (new File(repositoryConfig.getDirectory(), ".gitmodules").isFile()) {
      activity(Action.UPDATE_SUBMODULES, "update submodules");
      gitAdapter.syncAndUpdateSubmodules();
    }
  }

  private boolean determineUpstreamIsAvailable(Upstream upstream) {
    if (null == upstream) {
      return false;
    }

    List<String> branchNames = remoteBranchNames.get(upstream.getRemoteName());

    return null != branchNames && branchNames.contains(upstream.getRemoteBranch());
  }

  /**
   * Restore original HEAD state.
   */
  private void restoreHead() throws GitAPIException {
    if (null == headSymbolicRef) {
      return;
    }

    activity(Action.RESTORE_HEAD, "restore {}", headSymbolicRef);
    gitAdapter.checkout(headSymbolicRef);
  }

  /**
   * Record an activity to the journal and into the log.
   *
   * @param action    The performed action.
   * @param message   The message.
   * @param arguments Multiple message arguments.
   */
  private void activity(Action action, String message, Object... arguments) {
    message = MessageFormatter.arrayFormat(message, arguments).getMessage();
    logger.debug("[{}] {}: {}", repositoryConfig.getPathName(), action, message);
    Activity activity = new Activity(action, message);
    journal.add(activity);

    for (WorkerObserver observer : observers) {
      observer.activity(activity, this);
    }
  }

  public static class Stats {
    private int commitsBehind = 0;
    private int commitsAhead = 0;
    private int added = 0;
    private int modified = 0;
    private int changed = 0;
    private int deleted = 0;
    private boolean clean = true;

    public int getCommitsBehind() {
      return commitsBehind;
    }

    public int getCommitsAhead() {
      return commitsAhead;
    }

    public int getAdded() {
      return added;
    }

    public int getModified() {
      return modified;
    }

    public int getChanged() {
      return changed;
    }

    public int getDeleted() {
      return deleted;
    }

    public boolean isEmpty() {
      return 0 == commitsBehind
          && 0 == commitsAhead
          && isClean();
    }

    public boolean isClean() {
      return clean;
    }
  }
}
