package io.bit3.mgpm.worker;

import org.eclipse.jgit.lib.Ref;

public class FromToIsh {
  private final Ref from;
  private final Ref to;

  public FromToIsh(Ref from, Ref to) {
    this.from = from;
    this.to = to;
  }

  public Ref getFrom() {
    return from;
  }

  public Ref getTo() {
    return to;
  }
}
