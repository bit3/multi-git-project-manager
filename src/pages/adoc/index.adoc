= Multi Git Project Manager

Download (clone) and Update (pull) a mass of git repositories.

// == Configuration
//
// *TODO*

// == Usage
//
// *TODO*

// == Debian / Ubuntu Repository
//
// [source,bash]
// ----
// sudo apt-add-repository 'https://bit3.gitlab.io/multi-git-project-manager/ main'
// ----

== Downloads

- Debian / Ubuntu: link:pool/main/m/{deb}[{deb}]
- Windows: link:pool/main/m/{msi}[{msi}]
- Universal: link:pool/main/m/{tbz2}[{tbz2}] | link:pool/main/m/{zip}[{zip}]

== mgpm.yml reference

[source,yml]
----
github:
  token: abcdef0123456789...

repositories:
  -
    # generic git repository
    type: git
    name: acme # name of the directory
    path: example # (optional) sub-path inside working directory
    url: http://example.com/acme.git

  -
    # cgit provider
    type: cgit
    baseUrl: http://cgit.my.host # base url (index) to your cgit
    pathPrefix: /projects/acme/ # path prefix of scrapped repositories
    path: example # (optional) sub-path inside working directory

  -
    # github provider
    type: github
    owner: acme # name of the owner
    name: 'foo|bar' # (optional) name of projects, regexp pattern are allowed
    path: example # (optional) sub-path inside working directory

  -
    # gitlab provider
    type: gitlab
    url: http://example.com/gitlab # the domain for gitlab, if necessary with deployment path
    token: abcdef0123456789... # the users private token
    namespace: acme # name of the namespace, usually a user or group name
    project: 'foo|bar' # (optional) name of projects, regexp pattern are allowed
    archived: true # (optional) include archived projects, default is false
    path: example # (optional) sub-path inside working directory
----
