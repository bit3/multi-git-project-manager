import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import com.netflix.gradle.plugins.deb.Deb
import org.gradle.api.internal.file.copy.CopySpecWrapper
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

plugins {
    `java`
    `kotlin-dsl`
    `application`
    id("ca.cutterslade.analyze") version "1.4.0"
    id("net.researchgate.release") version "2.8.1"
    id("com.github.ben-manes.versions") version "0.28.0"
    id("nebula.ospackage-application") version "8.1.0"
}

//apply from: "gradle/windows.gradle"

group = "io.bit3"

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

application {
    mainClassName = "io.bit3.mgpm.App"
}

repositories {
    mavenCentral()
    maven {
        url = URI("https://gitlab.com/api/v4/projects/17382956/packages/maven")
    }
}

dependencies {
    permitUnusedDeclared("org.jetbrains.kotlin:kotlin-reflect")
    permitUnusedDeclared("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Logging libraries
    compile("org.slf4j:slf4j-api:1.7.26")
    compile("org.slf4j:slf4j-simple:1.7.26")
    permitUnusedDeclared("org.slf4j:slf4j-simple:1.7.26")

    // Commons libraries
    compile("commons-cli:commons-cli:1.4")
    compile("commons-lang:commons-lang:2.6")

    // CLI libraries
    compile("org.fusesource.jansi:jansi:1.17.1")
    compile("org.jline:jline:3.10.0")

    // jsch libraries
    compile("com.jcraft:jsch:0.1.54")
    compile("com.jcraft:jsch.agentproxy.jsch:0.0.9")
    compile("com.jcraft:jsch.agentproxy.core:0.0.9")
    compile("com.jcraft:jsch.agentproxy.sshagent:0.0.9")
    compile("com.jcraft:jsch.agentproxy.pageant:0.0.9")
    compile("com.jcraft:jsch.agentproxy.usocket-nc:0.0.9-p1")
    compile("com.jcraft:jsch.agentproxy.usocket-jna:0.0.9")
    permitUnusedDeclared("com.jcraft:jsch.agentproxy.usocket-jna:0.0.9")

    // GIT libraries
    compile("org.eclipse.jgit:org.eclipse.jgit:5.2.1.201812262042-r")

    // other libraries
    compile("org.yaml:snakeyaml:1.23")
    compile("org.eclipse.mylyn.github:org.eclipse.egit.github.core:2.1.5")
    compile("org.gitlab:java-gitlab-api:4.0.0")
    compile("org.jsoup:jsoup:1.11.3")
}

fun isNonStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.toUpperCase().contains(it) }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = stableKeyword || regex.matches(version)
    return isStable.not()
}

tasks.withType<DependencyUpdatesTask> {
    gradleReleaseChannel = "current"

    rejectVersionIf {
        isNonStable(candidate.version)
    }
}

tasks.named("distTar", Tar::class).configure {
    compression = Compression.BZIP2
}

ospackage {
    packageName = "mgpm"
    os = org.redline_rpm.header.Os.LINUX

    maintainer = "Tristan Lins <tristan@lins.io>"
    signingKeyId = "3AE12E05"
    url = "https://gitlab.com/bit3/multi-git-project-manager/"

    into("/opt/mgpm")

    from(tasks.getByName("startScripts", CreateStartScripts::class).outputs.files, closureOf<CopySpecWrapper> {
        uid = 0
        gid = 0
        into("bin")
    })
    from(tasks.getByName("jar", Jar::class).outputs.files, closureOf<CopySpecWrapper> {
        uid = 0
        gid = 0
        into("lib")
    })
    from(configurations.runtime, closureOf<CopySpecWrapper> {
        uid = 0
        gid = 0
        into("lib")
    })
}

val distDeb by tasks.creating(Deb::class) {
    requires("default-jre")
    link("/usr/bin/mgpm", "/opt/mgpm/bin/mgpm")
}
tasks.getByName("assemble").dependsOn(distDeb)
